import * as React from "react";

import "./ListItem.css"
interface IListItem {
    id: string;
    name: string;
    image_url: string;
    description: string;
}

const ListItem: React.StatelessComponent<IListItem> = (props) => {
    return (
        <div className="list-item">
            <div className="list-image" style={{backgroundImage: `url(${props.image_url})`}}/>
            <div>
                <h1>{props.name}</h1>
                <p>{props.description}</p>
            </div>

        </div>
    )
};

export default ListItem
