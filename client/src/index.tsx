import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import * as React from 'react';
import { ApolloProvider } from "react-apollo";
import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const link = createHttpLink({
    uri: "http://localhost:4000"
});

const client = new ApolloClient({
    cache: new InMemoryCache(),
    connectToDevTools: true,
    link,
    ssrMode: true,
});

hydrate(
    <ApolloProvider client={client}>
        <BrowserRouter>
           <App/>
        </BrowserRouter>
    </ApolloProvider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
