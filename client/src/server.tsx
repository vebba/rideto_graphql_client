import {InMemoryCache} from "apollo-cache-inmemory";
import {ApolloClient} from 'apollo-client';
import {createHttpLink} from 'apollo-link-http';
import * as express from 'express';
import * as React from 'react';
import {ApolloProvider} from 'react-apollo';
import * as ReactDomServer from 'react-dom/server';
import {StaticRouter} from 'react-router';
import Layout from './routes/Layout';



const app = express();
app.use((req, res) => {
    const client = new ApolloClient({
        cache: new InMemoryCache(),
        link: createHttpLink({
            credentials: 'same-origin',
            headers: {
                cookie: req.header('Cookie'),
            },
            uri: 'http://localhost:3000',
        }),
        ssrMode: true,
    });

    const context = {};

    const App = (
        <ApolloProvider client={client}>
            <StaticRouter location={req.url} context={context}>
                <Layout/>
            </StaticRouter>
        </ApolloProvider>
    );

    const html = ReactDomServer.renderToString(App);

    res.send(html);
});

app.listen(3001, () => console.log(`app Server is now running on http://localhost:${3001}`)); // tslint-disable-line no-console
