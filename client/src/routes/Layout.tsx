import * as React from 'react';
import { Route, Switch } from 'react-router';
import { Link } from 'react-router-dom';
import '../App.css'

// A Routes file is a good shared entry-point between client and server
import routes from '.';

const Layout = () =>
    <div className="App">
        <nav>
            <img src="https://d35oev165hap4m.cloudfront.net/static/bundles/img/logo/d76f48e860d582816ce49de8ade8c217.png"/>
            <ul>
                <li>
                    <Link to="/">Products</Link>
                </li>
                <li>
                    <Link to="/another">Another page</Link>
                </li>
            </ul>
        </nav>

        {/* New <Switch> behavior introduced in React Router v4
       https://reacttraining.com/react-router/web/api/Switch */}
        <Switch>
            {routes.map(route => <Route key={route.name} {...route} />)}
        </Switch>
    </div>;

export default Layout;
