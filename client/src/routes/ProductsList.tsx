import * as React from 'react';

import gql from 'graphql-tag';
import {Query} from 'react-apollo';
import ListItem from "../components/ListItem";
import "./ProductList.css";

const GET_PRODUCTS = gql`
    {
        products {
            model
            pk
            fields{
                name
                image_url
                description
            }
        }
    }
`;
const ProductList = () => (
    <Query query={GET_PRODUCTS}>
        {({loading, error, data}) => {
            if (loading) {
                return 'Loading...';
            }
            if (error) {
                return `Error! ${error.message}`;
            }
            return (

                <div>
                    {data.products.map((product: any, index: number) => (
                        <ul key={index} className="product-list">
                            <li><ListItem description={product.fields.description} image_url={product.fields.image_url} id={product.pk} name={product.fields.name}/></li>
                        </ul>
                    ))}
    1
                </div>
            );
        }}
    </Query>
);

export default ProductList;
