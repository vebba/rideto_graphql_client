import ProductList from "./ProductsList";

const routes = [
    {
        component: ProductList,
        exact: true,
        name: 'products',
        path: '/',
    },
];

export default routes;
