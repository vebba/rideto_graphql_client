import * as React from 'react';
import './App.css';
import Layout from "./routes/Layout";

const App = () => (<Layout/>);

export default App;
